const cookieContainer = document.querySelector(".cookie-container");
const cookieButton = document.querySelector(".cookie-btn");

cookieButton.addEventListener("click", () => {
    cookieContainer.classList.remove("active");
    localStorage.setItem("cookieBannerDisplayed", "true");
});

setTimeout( () => {
    if(!localStorage.getItem("cookieBannerDisplayed"))
    cookieContainer.classList.add("active");
}, 2000);

// Eine kleine Funktion zur Überprüfung von der Länge Zahlen. Quelle: https://www.w3resource.com/javascript-exercises/javascript-math-exercise-41.php
function digits_count(n) {
    var count = 0;
    if (n >= 1) ++count;

    while (n / 10 >= 1) {
        n /= 10;
        ++count;
    }

    return count;
}

// Eine Funktion die sich einen vom User eingegebenen Sitzungs-Code holt, ihn auf passende Länge überprüft und dementsprechend eine Meldung auslöst oder eine neue Seite öffnet.
$("#join").on("click", function () {
    var sitzungsCode = $("input[name*=sitzungsId]").val();
    if(digits_count(sitzungsCode) == 8) {
        window.open("https://frag.jetzt/participant/room/" +
            sitzungsCode, "_blank");
    }
    else {
        alert("Ihr Sitzungs-Code war nur " + digits_count(sitzungsCode) + " Zahlen lang. Ein gültiger Code muss 8 Zahlen lang sein.")
    }
});

// Variable die für die nächste Funktion gebraucht wird.
var toggle = 0;

// Diese Funktion wechselt zwischen 3 verschiedenen Bildern auf dem schwarzen Handy.
$("#webappOnPhone").on("click", function () {
    if(toggle == 0) {
        document.getElementById('phonePicture').src='img/phoneDifferentContrast.jpg';
    }
    else if(toggle == 1) {
        document.getElementById('phonePicture').src='img/jokePhonePage.jpg';
    }
    else {
        document.getElementById('phonePicture').src='img/iPhonePlus.jpg';
    }
    toggle++;
    toggle = toggle % 3;
});

